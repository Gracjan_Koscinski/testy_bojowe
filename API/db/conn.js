const { MongoClient } = require("mongodb");

const uri = process.env.MONGO_URI;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

let _db;

const connectDb = async () => {
  try {
    await client.connect();
    _db = client.db();
    console.log("Successfully connected to MongoDB");
  } catch (error) {
    console.error(error);
  }
};

const getDb = () => _db;

module.exports = {
  connectDb,
  getDb,
};
