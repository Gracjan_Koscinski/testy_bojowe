const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const { ObjectId } = require("mongodb");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Rejestracja użytkownika
recordRoutes.route("/register").post(async function (req, res) {
  try {
    const { name, email, password } = req.body;

    // Sprawdź, czy użytkownik już istnieje
    const userExists = await dbo.getDb().collection("users").findOne({ email });

    if (userExists) {
      return res.status(400).json({ message: "Użytkownik już istnieje" });
    }

    // Szyfruj hasło przed zapisaniem do bazy danych
    const hashedPassword = await bcrypt.hash(password, 10);

    // Zapisz nowego użytkownika do bazy danych
    await dbo
      .getDb()
      .collection("users")
      .insertOne({ name, email, password: hashedPassword });

    res.status(201).json({ message: "Rejestracja udana" });
  } catch (error) {
    console.error("Błąd podczas rejestracji:", error);
    res.status(500).json({ message: "Błąd podczas rejestracji" });
  }
});

// Logowanie użytkownika
recordRoutes.route("/login").post(async function (req, res) {
  try {
    const { email, password } = req.body;

    // Sprawdź, czy użytkownik istnieje
    const user = await dbo.getDb().collection("users").findOne({ email });

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return res
        .status(401)
        .json({ message: "Nieprawidłowy e-mail lub hasło" });
    }

    // Wygeneruj token JWT
    const token = jwt.sign(
      { id: user._id, email: user.email },
      process.env.JWT_SECRET,
      { expiresIn: "1h" } //czas wygaśnięcia tokena (1 godzina)
    );

    res.json({ token });
  } catch (error) {
    console.error("Błąd podczas logowania:", error);
    res.status(500).json({ message: "Błąd podczas logowania" });
  }
});

recordRoutes.route("/data").get(authenticateToken, (req, res) => {
  // Tu wiadomo że normalnie z bazy danych biorę coś
  res.json({ message: "Dane dostępne tylko dla zalogowanych użytkowników" });
});

// Middleware do weryfikacji JWT tokena
function authenticateToken(req, res, next) {
  const token = req.header("Authorization");

  if (!token) {
    return res.status(401).json({ message: "Brak tokena, nieautoryzowany" });
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
    if (err) {
      return res.status(403).json({ message: "Nieprawidłowy token" });
    }

    req.user = user;
    next();
  });
}

module.exports = recordRoutes;
