import React, { useState } from "react";
import axios from "axios";

const LoginForm = ({ onLogin }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(
        "http://localhost:5000/login",
        formData
      );
      const { token } = response.data;

      // Zapisz token w pamięci przeglądarki
      localStorage.setItem("token", token);

      // Wywołaj funkcję przekazaną przez prop, aby zaktualizować stan aplikacji
      onLogin();
    } catch (error) {
      console.error("Błąd podczas logowania:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="email">E-mail:</label>
      <input
        type="email"
        id="email"
        name="email"
        onChange={handleChange}
        required
      />

      <label htmlFor="password">Hasło:</label>
      <input
        type="password"
        id="password"
        name="password"
        onChange={handleChange}
        required
      />

      <button type="submit">Zaloguj</button>
    </form>
  );
};

export default LoginForm;
