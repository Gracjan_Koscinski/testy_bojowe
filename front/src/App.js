import React, { useState, useEffect } from "react";
import axios from "axios";
import LoginForm from "./LoginForm";
import RegistrationForm from "./RegistrationForm";

const App = () => {
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    // Sprawdzaj, czy użytkownik jest zalogowany po odświeżeniu strony
    checkLoginStatus();
  }, []);

  const checkLoginStatus = () => {
    const token = localStorage.getItem("token");
    if (token) {
      setLoggedIn(true);
    }
  };

  const handleLogin = () => {
    setLoggedIn(true);
  };

  const handleLogout = () => {
    // Usuń token z pamięci przeglądarki
    localStorage.removeItem("token");
    setLoggedIn(false);
  };

  const fetchData = async () => {
    try {
      const response = await axios.get("http://localhost:5000/data", {
        headers: { Authorization: localStorage.getItem("token") },
      });
      console.log(response.data);
    } catch (error) {
      console.error("Błąd podczas pobierania danych:", error);
    }
  };

  return (
    <div>
      {loggedIn ? (
        <>
          <p>Witaj! Jesteś zalogowany.</p>
          <button onClick={handleLogout}>Wyloguj</button>
          <button onClick={fetchData}>Pobierz Dane</button>
        </>
      ) : (
        <>
          <LoginForm onLogin={handleLogin} />
          <RegistrationForm />
        </>
      )}
    </div>
  );
};

export default App;
