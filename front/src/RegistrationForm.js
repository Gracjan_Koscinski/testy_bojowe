import React, { useState } from "react";
import axios from "axios";

const RegistrationForm = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      // Wysłanie danych do serwera
      const response = await axios.post(
        "http://localhost:5000/register",
        formData
      );

      // Otrzymanie odpowiedzi od serwera
      console.log(response.data);
    } catch (error) {
      console.error("Błąd podczas rejestracji:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="name">Imię:</label>
      <input
        type="text"
        id="name"
        name="name"
        onChange={handleChange}
        required
      />

      <label htmlFor="email">E-mail:</label>
      <input
        type="email"
        id="email"
        name="email"
        onChange={handleChange}
        required
      />

      <label htmlFor="password">Hasło:</label>
      <input
        type="password"
        id="password"
        name="password"
        onChange={handleChange}
        required
      />

      <button type="submit">Zarejestruj się</button>
    </form>
  );
};

export default RegistrationForm;
